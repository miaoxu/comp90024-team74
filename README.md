# COMP90024-Team74

## MRC Deployment
Before running any playbook script, users should generate a key pair on MRC and put the private key together with the openrc file into the root path of folder "Ansible".

### Instance Setup
Use Ansible to setup instances and build applications, this includes:

- create instances(including setup of security groups and volumes attachment) on MRC
- install common dependencies on all hosts
- mount volumes on couchdb hosts
- install docker on hosts where it will be used (mastodon & couchdb)
- build mastodon harvester docker image on mastodon host
- build couchdb on couchdb hosts
- setup couchdb clusters
- build web application on web host
```
./setup-instance.sh
```

### Start Mastodon Harvesters
Prerequisite: Add the ip of mastodon instance to the "mastodon" section in the 'hosts' file
Run below command to start harvesters:
```
./harvester-up.sh
```
For different Mastodon servers, change the "mastodon_server" variable in the shell script:
- "au": mastodon.au
- "social": mastodon.social

### Stop Mastodon Harvesters
Prerequisite: Add the ip of mastodon instance to the "mastodon" section in the 'hosts' file
Run below command to stop a harvester:
```
./harvester-down.sh
```
For different Mastodon servers, change the "mastodon_server" argument in the shell script:
- "au": mastodon.au
- "social": mastodon.social

### Update Mastodon Harvester Image
Run the script below to update Mastodon Harvester after the application files are modified
```
./update-mastodon-harvester.sh
```

### Add a node to a CouchDB cluster
Prerequisite: Add the ip of CouchDB master node to the "db-master" section in the 'hosts' file
```
./update-mastodon-harvester.sh
```


=============================================================================================================
## Data preprocessing 

### Add your files

put raw tweet file into file folder

### Run

drafts.py

add_geo.ipynb

sentence_to_token.py

translate.py

the final output file is final.json


=============================================================================================================
## Visualization

### Add your files

put the following files into file folder:

get geo data from:
    sal.json
    SAL_2021_AUST.xlsx
    MB_2021_AUST.xlsx
    geo/...<geo shp files>
    source: https://www.abs.gov.au/

get salary data from:
    abs_2021_salary.csv
    source: https://sudo.eresearch.unimelb.edu.au/

get education data from:
    abs_2021_labour_education.csv
    source: https://sudo.eresearch.unimelb.edu.au/

### Process Sudo data
convertSUDO2SAL.ipynb

calculateEduScore.ipynb

to get SAL mapping SUDO dataset for salary and education data

### Mapreduce source file
gambling_area_vulgarity.json

gambling_language.json

gambling_type_sentiment.json

gcc_count.json

mastodon_count.json

mastodon_gambling_language.json

mastodon_gambling_sentiment.json

politics_area.json

preferred_hours.json

vulgarity_sentiment_area.json

vulgarity_sentiment_suburb.json

### Run
plot_map_reduce1.ipynb and plot_map_reduce2.ipynb to produce data visualization for mapreduce results

plot_map.ipynb to produce 3 maps: tweets with vulgar distribution across Australia, Education across Australia and median tot

plot.py can automatically retrieve mapreduce results from couchDB and produce data visualization
