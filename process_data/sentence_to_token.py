from gensim.utils import tokenize
import ijson
import json
import thulac	
from thai_tokenizer import Tokenizer
import fugashi

# take sereval mins

#data_url = 'result_small.json'
#output = 'final_small.json'

data_url = '../file/result.json'
output = '../file/final.json'

lang_list=['en','hu', 'mr', 'tl', 'vi', 'fr', 'da', 'lt', 'tr', 'my', 'ca', 'ru', 'ta', 'ro', 'sl', 'kn', 'bg', 'sd', 'is', 'de', 'am', 'zh', 'iw', 'nl', 'th', 'es', 'lv', 'ko', 'te', 'hy', 'it', 'ne', 'et', 'cs', 'gu', 'ja', 'cy', 'ml', 'pl', 'ar', 'pt', 'bn', 'ps', 'uk', 'el', 'ka', 'no', 'hi', 'fa', 'pa', 'fi', 'sr', 'eu', 'si', 'sv', 'ht', 'ur']
#extra_attention=['zh', 'th','ja']



f = open('../file/gambling.json', encoding='utf-8')
gambling_vocab = json.load(f)
gambling_vocab['en']

f = open('../file/vulgar.json', encoding='utf-8')
vulgar_vocab = json.load(f)
vulgar_vocab['en']

thu1 = thulac.thulac(seg_only=True)  # chinese
tokenizer = Tokenizer() # taiwen
tagger = fugashi.Tagger() # japan

dict_list = [] 
with open(data_url, encoding='utf-8') as ff:
    tweets = ijson.items(ff,"tweets.item")
    for i in tweets:
        i['gambling'] = 0
        i['vulgar'] = 0
        i['token'] = []
        if i['language'] == 'zh':
            i['token'] = [i[0] for i in thu1.cut(i['text']) ] 
        elif i['language'] == 'th':
            i['token'] = tokenizer.split(i['text'])
        elif i['language'] == 'ja':
            i['token'] = [word.surface for word in tagger(i['text'])]
        elif i['language'] in lang_list:
            i['token'] = list(set(list(tokenize(i['text'])))) # fast

        ind=0
        while (i['gambling'] == 0 or i['vulgar'] == 0) and ind < len(i['token']):
            word = i['token'][ind]
            if word in gambling_vocab[i['language']]:
                i['gambling'] = 1
            if word in vulgar_vocab[i['language']]:
                i['vulgar'] = 1
            ind +=1
        dict_list.append(i)

dd={}
dd['tweets'] = dict_list
with open(output, 'w',encoding='utf-8') as f:
    json.dump(dd, f, ensure_ascii=False, default=str)
 





