import json
from googletrans import Translator

data_url = '../file/result.json'


gambling_list =['bets', 'wagers', 'wager', 'gambled', 'gamble', 'bingo', 'betting', 'casino', 'poker', 'gambler', 'lotteries', 'pokie', 'wagering', 'bookmaking', 'craps', 'bet', 'casinos', 'blackjack', 'gambling', 'keno', 'wagered', 'gaming', 'bettors', 'bookmaker', 'horseracing', 'gambles', 'baccarat', 'gamblers']
vul_list=['arse',
'arsehead',
'arsehole',
'ass',
'asshole',
'bastard',
'bitch',
'bloody',
'bollocks',
'brotherfucker',
'bugger',
'bullshit',
'child-fucker',
'cock',
'cocksucker',
'crap',
'cunt',
'damn',
'dick',
'dickhead',
'dyke',
'fatherfucker',
'frigger',
'fuck',
'goddamn',
'godsdamn',
'hell',
'horseshit',
'kike',
'motherfucker',
'nigga',
'nigra',
'piss',
'prick',
'pussy',
'shit',
'shite',
'sisterfucker',
'slut',
'spastic',
'turd',
'twat',
'wanker',
'arseheads',
'bastards',
'bitches',
'bloody',
'bollocks',
'cocks',
'dicks',
'fucking',
'pisses',
'pussies',
'sluts',
'turds',
'wankers']
lang_list_no_en=['hu', 'mr', 'tl', 'vi', 'fr', 'da', 'lt', 'tr', 'my', 'ca', 'ru', 'ta', 'ro', 'sl', 'kn', 'bg', 'sd', 'is', 'de', 'am', 'zh', 'iw', 'nl', 'th', 'es', 'lv', 'ko', 'te', 'hy', 'it', 'ne', 'et', 'cs', 'gu', 'ja', 'cy', 'ml', 'pl', 'ar', 'pt', 'bn', 'ps', 'uk', 'el', 'ka', 'no', 'hi', 'fa', 'pa', 'fi', 'sr', 'eu', 'si', 'sv', 'ht', 'ur']

translator = Translator()


vocab_dict={}
vocab_dict['en'] = gambling_list
for lang in lang_list_no_en:
    if lang =='zh':
        vocab_dict[lang] = [i.text for i in translator.translate(gambling_list,dest='zh-cn')]
    else:
        vocab_dict[lang] = [i.text for i in translator.translate(gambling_list,dest=lang)]




with open('../file/gambling.json', 'w',encoding='utf-8') as f:
    json.dump(vocab_dict, f, ensure_ascii=False, default=str)


vocab_dict={}
vocab_dict['en'] = vul_list
for lang in lang_list_no_en:
    if lang =='zh':
        vocab_dict[lang] = [i.text for i in translator.translate(vul_list,dest='zh-cn')]
    else:
        vocab_dict[lang] = [i.text for i in translator.translate(vul_list,dest=lang)]




with open('../file/vulgar.json', 'w',encoding='utf-8') as f:
    json.dump(vocab_dict, f, ensure_ascii=False, default=str)

