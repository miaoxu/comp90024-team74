import http.client as client
import json
import base64

username = 'admin'
password = 'admin'

credentials = base64.b64encode(f"{username}:{password}".encode('utf-8')).decode('utf-8')

headers = { 'Authorization' : f'Basic {credentials}' }
conn = client.HTTPConnection("172.26.133.52", 5984)
def request():
    conn.request("GET", "/tweet/_design/gcc/_view/gcc_count?reduce=true&group_level=2", headers=headers)
    body = conn.getresponse().read().decode()
    data = json.loads(body)
    return data

