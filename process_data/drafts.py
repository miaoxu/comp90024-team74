## Author: Xu Miao
import ijson
import pandas as pd

data_url = '../file/twitter-huge.json'

data_list = []
data_with_geo_list = []
data_list_count = 1
data_list_with_geo_count = 1
with open(data_url, 'r', encoding='utf-8') as f:
    count_total = 0
    count_geo = 0
    tweets = ijson.items(f, 'rows.item', buf_size=20480)
    for i in tweets:
        count_total += 1
        author_id = i['doc']['data']['author_id']
        create_time = i['doc']['data']['created_at']
        language = i['doc']['data']['lang']
        text = i['doc']['data']['text']
        token = i['value']['tokens']
        sentiment = i['doc']['data']['sentiment']
        try:
            tag = i['doc']['matching_rules'][0]['tag']
        except KeyError:
            tag = ''
        try:
            location = i['doc']['includes']['places'][0]['full_name']
            count_geo += 1
        except (KeyError, TypeError):
            location = ''

        # if count >= 10:
        #     break
        output = {
            'usr_id': author_id,
            'create_time': create_time,
            'token': token,
            'tag': tag,
            'text': text,
            'language': language,
            'location': location,
            'sentiment': sentiment
        }
        data_list.append(output)
        if location:
            data_with_geo_list.append(output)

        # print(len(data_list))
        # print(len(data_with_geo_list))

        if len(data_with_geo_list) >= 1000000:
            pd.DataFrame(data_with_geo_list).to_csv('../file/big_twitter_geo_{}.csv'.format(data_list_with_geo_count), index=False)
            data_list_with_geo_count += 1
            data_with_geo_list = []


pd.DataFrame(data_with_geo_list).to_csv('../file/big_twitter_geo_{}.csv'.format(data_list_with_geo_count), index=False)


print(count_total)
print(count_geo)


