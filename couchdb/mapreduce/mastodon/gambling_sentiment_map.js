function(doc) {
    if (doc.language == 'en' && doc.gambling == 1) {
        emit(doc.source, doc.sentiment);
    }
}