function(keys, values, rereduce) {
    var result = {gambling: 0, vulgar: 0, tweets: 0};

    for(var i = 0; i < values.length; i++) {
        if(values[i].gambling == 1) {
            result.gambling++;
        }
        if(values[i].vulgar == 1) {
            result.vulgar++;
        }
        result.tweets += values[i].tweet;
    }

    return result;
}