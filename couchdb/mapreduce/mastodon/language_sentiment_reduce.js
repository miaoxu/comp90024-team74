function(keys, values, rereduce) {
    var result = {count: 0, sentiment: 0.0};
    values.forEach(function(value) {
        result.count += value.count;
        result.sentiment += value.sentiment;
    });
    return result;
}