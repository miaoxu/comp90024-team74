// Define map function
function(doc) {
    if (doc.gambling === 1) {
        var words = ["Albanese", "Morrison", "Bandt", "election", "Grayndler", "Labor", "Liberal", "National Coalition", "Seats won", "House of Representatives", "government", "Senate", "Parliament"];
        for (var i in words) {
            if (doc.text.toLowerCase().includes(words[i])) {
                // Emit the word, and an object with the sentiment and a count of 1
              emit(doc.state, 1);
            }
        }
    }
}