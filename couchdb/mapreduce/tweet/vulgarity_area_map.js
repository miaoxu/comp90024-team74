// Define map function
function(doc) {
    emit(doc.state + ", " + doc.suburb, {count: 1, vulgarity: doc.vulgar, sentiment: parseFloat(doc.sentiment)});
}
