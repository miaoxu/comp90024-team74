// Define map function
function(doc) {
    // Check if the tweet is related to gambling
    if (doc.gambling === 1) {
        // Emit the location (state and suburb) as the key, and an object with count and vulgarity as the value
        emit(doc.state + ", " + doc.suburb, {count: 1, vulgarity: doc.vulgar});
    }
}