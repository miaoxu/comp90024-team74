// Define reduce function
function(keys, values, rereduce) {
    var result = {count: 0, vulgarity: 0.0, sentiment: 0.0};
    for (var i in values) {
        result.count += values[i].count;
        result.vulgarity += values[i].vulgarity;
        result.sentiment += values[i].sentiment; 
    }
    return result;
}