// Define map function
function(doc) {
    if (doc.gambling === 1) {
        var words = ["horse", "soccer", "football", "footy", "tennies", "crypto", "NFT", "bitcoin", "Ethereum", "Cryptocurrency","Altcoin,Stablecoin", "Decentralized finance", "Litecoin", "Token coin", "Binance,Altcoins,Tether","online", "crown", "sportsbet", "tab", "betfair", "bet365", "ladbrokes", "Keno", "casino", "poker", "lotto", "lottery", "dog race", "scratch tickets","Joe Fortune", "Ignition", "Red Dog", "Aussie Play", "Ricky Casino", "Hellspin"];
        for (var i in words) {
            if (doc.text.toLowerCase().includes(words[i])) {
                // Emit the word, and an object with the sentiment and a count of 1
                emit(words[i], {count: 1, totalSentiment: parseFloat(doc.sentiment)});
            }
        }
    }
}