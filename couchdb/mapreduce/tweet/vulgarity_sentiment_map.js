function(doc) {
    if (doc.language && doc.sentiment && doc.vulgar) {
        var sentiment = parseFloat(doc.sentiment);
        if (isNaN(sentiment)) {
            sentiment = 0;
        }
        emit(doc.language, {sentiment: sentiment, vulgar: doc.vulgar});
    }
}