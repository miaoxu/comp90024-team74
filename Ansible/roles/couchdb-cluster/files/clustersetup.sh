#!/usr/bin/env bash
# from https://sharats.me/posts/shell-script-best-practices/

set -o errexit
set -o nounset
if [[ "${TRACE-0}" == "1" ]]; then
    set -o xtrace
fi

if [[ "${1-}" =~ ^-*h(elp)?$ ]]; then
    echo 'Usage: ./clustersetup.sh <master-node-ip> <node-ip> <node-ip> ...
'
    exit
fi

cd "$(dirname "$0")"
user="admin"
pass="admin"
nodes=($@)
masternode="${nodes[0]}"
othernodes=("${nodes[@]:1}")
nnode=${#nodes[@]}

echo "masternode: " "$masternode"
echo "others: " "${othernodes[@]}"

curl -X POST -H "Content-Type: application/json"\
  http://"$user:$pass@$masternode":5984/_cluster_setup\
  -d '{"action": "enable_cluster",
	"bind_address":"0.0.0.0",
	"username": "admin", "password":"admin", "node_count": "'"${nnode}"'"}'

for node in "${othernodes[@]}"
do
    curl -X POST -H "Content-Type: application/json"\
      http://"$user:$pass@$node":5984/_cluster_setup\
      -d '{"action": "enable_cluster",
      "bind_address":"0.0.0.0", "username": "admin", "password":"admin", "node_count": "'"${nnode}"'"}'

    # https://github.com/apache/couchdb/issues/2858#issuecomment-663731643
    # Generate two UUIDs from the server and store in Bash array
    uuids=($(curl -s http://"$node":5984/_uuids?count=2 | jq -r '.uuids[]'))
    # Set the first UUID in the CouchDB configuration
    curl -X PUT "http://${user}:${pass}@${node}:5984/_node/_local/_config/couchdb/uuid" -d "\"${uuids[0]}\""
    # Set the second UUID as the shared http secret for cookie creation
    curl -X PUT "http://${user}:${pass}@${node}:5984/_node/_local/_config/chttpd_auth/secret" -d "\"${uuids[1]}\""

    curl -XPOST "http://${user}:${pass}@${masternode}:5984/_cluster_setup" \
      --header "Content-Type: application/json"\
      --data "{\"action\": \"enable_cluster\", \"bind_address\":\"0.0.0.0\",\
             \"username\": \"${user}\", \"password\":\"${pass}\", \"port\": \"5984\",\
             \"remote_node\": \"${node}\", \"node_count\": \"${nnode}\",\
             \"remote_current_user\":\"${user}\", \"remote_current_password\":\"${pass}\"}"

    curl -XPOST "http://${user}:${pass}@${masternode}:5984/_cluster_setup"\
      --header "Content-Type: application/json"\
      --data "{\"action\": \"add_node\", \"host\":\"${node}\",\
             \"port\": \"5984\", \"username\": \"${user}\", \"password\":\"${pass}\"}"
done

curl -XPOST "http://${user}:${pass}@${masternode}:5984/_cluster_setup"\
    --header "Content-Type: application/json" --data "{\"action\": \"finish_cluster\"}"

curl http://"$user:$pass@$masternode":5984/_cluster_setup
curl "http://${user}:${pass}@${masternode}:5984/_membership"
