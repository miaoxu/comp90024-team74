#!/usr/bin/env bash
# from https://sharats.me/posts/shell-script-best-practices/

set -o errexit
set -o nounset
if [[ "${TRACE-0}" == "1" ]]; then
    set -o xtrace
fi

if [[ "${1-}" =~ ^-*h(elp)?$ ]]; then
    echo 'Usage: sudo ./bootstrap.sh NODE_IP
Prerequisite: disk mounted at /mnt/data
'
    exit
fi

cd "$(dirname "$0")"

node=$1
containername=couchdb
user='admin'
pass='admin'
VERSION='3.2.1'
cookie='a192aeb9904e6590849337933b000c99'

docker pull ibmcom/couchdb3:"$VERSION"
docker stop "$containername" || true
docker rm "$containername" || true

# volume location: https://hub.docker.com/layers/ibmcom/couchdb3/3.2.1/images/sha256-b66325906a8dc8a018054e1b88a8162a5f939b82a6391490c5b07a44d7dcfdc9?context=explore
docker create\
  --name "$containername"\
  --env COUCHDB_USER="$user"\
  --env COUCHDB_PASSWORD="$pass"\
  --env COUCHDB_SECRET="$cookie"\
  --env ERL_FLAGS="-setcookie \"${cookie}\" -name \"couchdb@${node}\" -kernel inet_dist_listen_min 9100 -kernel inet_dist_listen_max 9200"\
  -p 5984:5984\
  -p 4369:4369\
  -p 5986:5986\
  -p 9100-9200:9100-9200\
  --user 0 `#run as root so that container has access to mounted folder` \
  -v /mnt/data:/opt/couchdb/data\
  ibmcom/couchdb3:"$VERSION"
docker start "$containername"
