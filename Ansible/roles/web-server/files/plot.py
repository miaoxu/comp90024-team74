import pandas as pd
import sys
import json
import geopandas as gpd
import folium
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter1d
import matplotlib.cm as cm


import http.client as client
import json
import base64
username = 'admin'
password = 'admin'
credentials = base64.b64encode(f"{username}:{password}".encode('utf-8')).decode('utf-8')
headers = { 'Authorization' : f'Basic {credentials}' }
conn = client.HTTPConnection(sys.argv[1], 5984)

def request(s):
    conn.request("GET", s, headers=headers)
    return conn.getresponse().read().decode()

def request_data(s):
    conn.request("GET", s, headers=headers)
    body = conn.getresponse().read().decode()
    data = json.loads(body)
    return data
#data_url = '../file/vulgarity_sentiment_suburb.json'

#with open(data_url, 'r', encoding='utf-8') as f:
#    twitter_file = json.load(f)
twitter_file = request_data("/tweet/_design/vulgarity/_view/vulgarity_area?reduce=true&group_level=2")
twitter_file = twitter_file['rows']
df1 = pd.DataFrame(twitter_file)
df1[['state','suburb']] = df1['key'].str.split(', ',expand=True)

df1 = pd.concat([df1.drop(['value'], axis=1), df1['value'].apply(pd.Series)], axis=1)
df1['suburb'] = df1['suburb'].apply(str. lower)
df1 = df1[df1['count'] > 300]

state_name = ["new south wales", "victoria", "queensland", "south australia", 
                "western australia", "tasmania", "northern territory", 
                "australian capital territory", 'great other territory']
gccs = ['1gsyd', '2gmel', '3gbri', '4gade', 
            '5gper', '6ghob', '7gdar', '8acte','9oter']

with open("../file/sal.json", "r") as f:
    sal_df = pd.DataFrame(json.load(f)).T
sal_df.index = sal_df.index.map(lambda x: x.split(" (")[0])  # remove braket
sal_df.index.name = "place"
sal_df['ste'] = sal_df['ste'].apply(lambda x: state_name[int(x)-1])
sal_df = sal_df.groupby(["place",'ste']).agg({'sal':list}).reset_index()
sal_df['sal'] = sal_df['sal'].apply(lambda x: x[0])
sal_df = sal_df.rename(columns={'place': 'suburb', 'ste': 'state', 'sal':'SAL_CODE_2021'})

merged_df = pd.merge(df1, sal_df, on=['state', 'suburb'],how='inner')
merged_df = merged_df.rename(columns={'count': 'total'})

merged_df = merged_df.assign(Vul_Percent=lambda x: x.vulgarity / x.total)



sf = gpd.read_file("../file/reduced_geo/r2SAL_2021_AUST_GDA2020.shp")
data = sf.rename(columns={'FID': 'SAL_CODE_2021'})

sf = gpd.read_file("../file/geo/SAL_2021_AUST_GDA2020.shp")
sf = sf.rename(columns={'SAL_CODE21': 'SAL_CODE_2021'})
data = gpd.GeoDataFrame({'SAL_CODE_2021':sf['SAL_CODE_2021'],'geometry':data['geometry']})

data["x"] = data["geometry"].centroid.x
data["y"] = data["geometry"].centroid.y
d = data.merge(merged_df, on='SAL_CODE_2021')


colors = ['darkgreen','green','orange','red','darkred']
d['quantile'] = pd.qcut(d['Vul_Percent'],q=len(colors),labels=False)

d['color'] = d['quantile'].apply(lambda x: colors[x])


m = folium.Map(location=[-27, 133], tiles="Stamen Terrain", zoom_start=6)

d.apply(lambda row:folium.Marker(location=[row["y"], row["x"]], 
                                 radius=10, 
                                 popup=[row['suburb'],row['Vul_Percent']],
                                 icon=folium.Icon(color=row.color)).add_to(m), axis=1)

m.save('../file/foliumChoroplethMapnew.html')

geoJSON = data[['SAL_CODE_2021', 'geometry']].drop_duplicates('SAL_CODE_2021').to_json()


pd1 = pd.read_csv('../file/sal_edu_score.csv')
pd1 = pd1.drop(pd1[pd1.score > 1].index)


m = folium.Map(location=[-30, 127], tiles="Stamen Terrain", zoom_start=10)
c = folium.Choropleth(
    geo_data=geoJSON, 
    name='choropleth',
    data=pd1.reset_index(), # data source
    columns=['SAL_CODE_2021','score'], # the columns required
    key_on='properties.SAL_CODE_2021', # this is from the geoJSON's properties
    fill_color='YlOrRd', 
    nan_fill_color='black',
    legend_name='Education across Australia',
)

c.add_to(m)
m.save('../file/foliumChoroplethMaped.html')

pd1 = pd.read_csv('../file/sal_salary.csv')
m = folium.Map(location=[-30, 127], tiles="Stamen Terrain", zoom_start=10)

c = folium.Choropleth(
    geo_data=geoJSON, 
    name='choropleth',
    data=pd1.reset_index(), # data source
    columns=['SAL_CODE_2021', ' median_tot_fam_inc_weekly'], # the columns required
    key_on='properties.SAL_CODE_2021', # this is from the geoJSON's properties
    fill_color='YlOrRd', 
    nan_fill_color='black',
    legend_name='median total family income weekly across Australia'
)

c.add_to(m)


# file_path = '../file/mastodon_gambling_sentiment.json'
# with open(file_path, 'r', encoding='utf-8') as f:
    # twitter_file = json.load(f)
twitter_file =request_data("/mastodon_p/_design/language/_view/language_sentiment?reduce=true&group_level=2")
df = pd.DataFrame(twitter_file['rows'])

df['value'] = df['value'].apply(lambda x: x['sentiment'] / x['count'])
df['value'] = df['value'].apply(lambda x:2*x-1)


plt.title('Average sentiment regarding to gambling by different server')
plt.bar(df['key'],df['value'])
plt.savefig('./images/toot_sentiment.png')


# file_path = '../file/politics_area.json'
# with open(file_path, 'r', encoding='utf-8') as f:
    # twitter_file = json.load(f)
twitter_file = request_data("/tweet/_design/politics/_view/politics_area?reduce=true&group_level=2")
df = pd.DataFrame(twitter_file['rows'])

plt.figure(figsize=(10,6))
plt.title('Number of tweets gambling about politics in each state')
plt.xticks(rotation=30)
for i, v in enumerate(df['value']):
    plt.text(i, v + 1, str(v), ha='center')
plt.bar(df['key'],df['value'])
plt.savefig('./images/tweet_politic.png')

# file_path = '../file/preferred_hours.json'
# with open(file_path, 'r', encoding='utf-8') as f:
    # twitter_file = json.load(f)
twitter_file = request_data("/tweet/_design/general/_view/preferred_hours?reduce=true&group_level=2")
df = pd.DataFrame(twitter_file['rows'])
df['key'] = df['key'].apply(lambda x: x + 10)
df['key'] = df['key'].apply(lambda x:  x - 24 if x > 23 else x)
df = df.sort_values(by=['key'])


ysmoothed = gaussian_filter1d(df['value'], sigma=2)
plt.figure(figsize=(10,6))
plt.title('Number of tweets made according to 24 hours in a day')
plt.plot(df['key'], ysmoothed, marker='o')
plt.savefig('./images/tweet_time.png')

# file_path = '../file/vulgarity_sentiment_area.json'
# with open(file_path, 'r', encoding='utf-8') as f:
    # twitter_file = json.load(f)
twitter_file = request_data("/tweet/_design/vulgar_sentiment/_view/vulgar_sentiment_area?reduce=true&group_level=2")
df = pd.DataFrame(twitter_file['rows'])
df['x'] = df['value'].apply(lambda x: x[2])
df = df.drop(df[df.x < 50].index)
df['vul_pro'] = df['value'].apply(lambda x: x[0] / x[2])
df['senti'] = df['value'].apply(lambda x: x[1] / x[2])


plt.figure(figsize=(10,6))
plt.title('Proportion of tweets with vulgar words in each state')
plt.xticks(rotation=30)
plt.bar(df['key'],df['vul_pro'])
plt.savefig('./images/tweet_state_vulgar.png')

plt.figure(figsize=(10,6))
plt.title('Average sentiment in each state')
plt.xticks(rotation=30)
plt.bar(df['key'],df['senti'])
plt.savefig('./images/state_sentiment.png')


# gam_lang = pd.read_json('../file/gambling_language.json')
# gam_type_sen = pd.read_json('../file/gambling_type_sentiment.json', orient='records')
# mas_cnt = pd.read_json('../file/mastodon_count.json')
# mas_gam_lang = pd.read_json('../file/mastodon_gambling_language.json')
# lang_code = pd.read_csv('../file/iso_639_1_code.csv')

gam_lang = pd.read_json(request("/tweet/_design/gambling/_view/gambling_language?reduce=true&group_level=2"))
gam_type_sen = pd.read_json(request("/tweet/_design/gambling/_view/gambling_type_sentiment?reduce=true&group_level=2"), orient='records')
mas_cnt = pd.read_json(request("/mastodon_p/_design/no_cal/_view/count?reduce=true&group_level=2"))
mas_gam_lang = pd.read_json(request("/mastodon_p/_design/gambling/_view/language?reduce=true&group_level=2"))
lang_code = pd.read_csv('../file/iso_639_1_code.csv')

df_gam_type_sen = pd.json_normalize(gam_type_sen['rows'])
df_gam_type_sen['avg sentiment'] = df_gam_type_sen['value.totalSentiment']/df_gam_type_sen['value.count']
df_gam_type_sen = df_gam_type_sen.rename(columns={'key': 'type'})

ax = df_gam_type_sen.plot.bar(x='type', y='avg sentiment', rot=0, figsize=(10, 5))
ax.set_xticklabels(ax.get_xticklabels(), rotation=45, ha='right')
plt.title('Average sentiment for each gambling')
ax.get_legend().remove()
plt.xlabel('')
plt.savefig('./images/keyword_sentiment.png')

#df.plot.pie(y='type', labels=df['value.count'], autopct='%1.1f%%')
cmap = cm.get_cmap('tab20')
ax = df_gam_type_sen.plot.pie(y='value.count', labels=[''] * len(df_gam_type_sen['value.count']), autopct='%1.1f%%', pctdistance=1.2, colors=cmap(range(len(df_gam_type_sen['value.count']))))
#ax.set_xticklabels(ax.get_xticklabels(), rotation=45, ha='right')

plt.legend(df_gam_type_sen['type'], loc='center left', bbox_to_anchor=(1.2, 0.5))
plt.title('Ratio for each gambling keywords')
plt.ylabel('')
plt.xlabel('')
plt.savefig('./images/tweet_keywords.png')

gam_list = ['lottery', 'lotto', 'poker']
df_gam = df_gam_type_sen[df_gam_type_sen['type'].isin(gam_list)]

ax = df_gam.plot.bar(x='type', y='value.count', rot=0)
ax.set_xticklabels(ax.get_xticklabels(), rotation=45, ha='right')
ax.get_legend().remove()
plt.title('Number of tweets in each gambling type')
plt.ylabel('')
plt.xlabel('')
plt.savefig('./images/tweet_sport.png')

sport_list = ['football', 'footy', 'soccer', 'horse']
df_sport = df_gam_type_sen[df_gam_type_sen['type'].isin(sport_list)]

ax = df_sport.plot.bar(x='type', y='value.count', rot=0)
ax.set_xticklabels(ax.get_xticklabels(), rotation=45, ha='right')
ax.get_legend().remove()
plt.title('Number of tweets in each sport type')
plt.ylabel('')
plt.xlabel('')
plt.savefig('./images/tweet_sport.png')




crypto_list = ['crypto', 'NFT', 'bitcoin', 'Ethereum', 'Cryptocurrency','Altcoin','Stablecoin', 'Decentralized finance', 'Litecoin', 'Token coin', 'Binance','Altcoins','Tether']
df_crypto = df_gam_type_sen[df_gam_type_sen['type'].isin(crypto_list)]

ax = df_crypto.plot.bar(x='type', y='value.count', rot=0)
ax.set_xticklabels(ax.get_xticklabels(), rotation=45, ha='right')
ax.get_legend().remove()
plt.title('Number of tweets in each crypto type')
plt.ylabel('')
plt.xlabel('')
plt.savefig('./images/tweet_crypto.png')

gam_place_list = ['online', 'crown', 'sportsbet', 'tab', 'betfair', 'bet365', 'ladbrokes', 'Keno', 'casino', 'dog race', 'scratch tickets','Joe Fortune', 'Ignition', 'Red Dog', 'Aussie Play', 'Ricky Casino', 'Hellspin']
df_gam_place = df_gam_type_sen[df_gam_type_sen['type'].isin(gam_place_list)]

ax = df_gam_place.plot.bar(x='type', y='value.count', rot=0)
ax.set_xticklabels(ax.get_xticklabels(), rotation=45, ha='right')
ax.get_legend().remove()
plt.title('Number of tweets in each gambling place')
plt.ylabel('')
plt.xlabel('')
plt.savefig('./images/tweet_place.png')

df_gam_lang = pd.json_normalize(gam_lang['rows'])
lang_code = lang_code.drop(['Native name (endonym)'],axis=1)
df_gam_lang_merg = pd.merge(df_gam_lang, lang_code, left_on='key', right_on='639-1 code')
df_gam_lang_merg = df_gam_lang_merg.drop(['key','639-1 code'], axis=1)

df_gam_lang_merg_sort = df_gam_lang_merg.sort_values(by='value', ascending=False)
top_values = df_gam_lang_merg_sort.head(10)
other_sum = df_gam_lang_merg_sort.iloc[10:]['value'].sum()
df_gam_lang_merg_sort_pie = pd.concat([top_values, pd.DataFrame({'ISO language name': ['Other'], 'value': [other_sum]})])

#ax = df_gam_lang_merg.plot.bar(x='ISO language name', y='value', rot=0, figsize=(10, 5))
cmap = cm.get_cmap('tab20')
ax = df_gam_lang_merg_sort_pie.plot.pie(y='value', labels=df_gam_lang_merg_sort_pie['ISO language name'], autopct='%1.1f%%', pctdistance=0.8, colors=cmap(range(len(df_gam_lang_merg_sort_pie['value']))))
ax.set_xticklabels(ax.get_xticklabels(), rotation=45, ha='right')
plt.title('Ratio of tweets related to gambling in each language')
ax.get_legend().remove()
plt.ylabel('')
plt.xlabel('')
plt.savefig('./images/tweet_lang.png')

# gam_area = pd.read_json('../file/gambling_area_vulgarity.json')
gam_area = pd.read_json(request("/tweet/_design/gambling/_view/gamling_area_vulgarity?reduce=true&group_level=2"))

df_gam_area = pd.json_normalize(gam_area['rows'])
df_gam_area[['state','suburb']] = df_gam_area['key'].str.split(', ',expand=True)

df_gam_area_state = df_gam_area.drop(['key', 'suburb', 'value.vulgarity'], axis=1)

df_gam_area_state_g = df_gam_area_state.groupby('state').sum().reset_index()
df_gam_area_state_g = df_gam_area_state_g.sort_values(by='state')

ax = df_gam_area_state_g.plot.bar(x='state', y='value.count', rot=0, figsize=(10, 5))
ax.set_xticklabels(ax.get_xticklabels(), rotation=45, ha='right')
ax.get_legend().remove()
#plt.title('The number of tweets related to gambiling in each language')
#plt.show()

ax = df_gam_area_state_g.plot.pie(y='value.count', labels=df_gam_area_state_g['state'], autopct='%1.1f%%', pctdistance=0.8)
#ax.set_xticklabels(ax.get_xticklabels(), rotation=45, ha='right')

plt.legend(df_gam_area_state_g['state'], loc='center left', bbox_to_anchor=(1.2, 0.5))
ax.get_legend().remove()
plt.title('Ratio for gambling related tweets in each state')
plt.ylabel('')
plt.xlabel('')
plt.savefig('./images/tweet_state.png')

df_mas_gam_lang = pd.json_normalize(mas_gam_lang['rows'])
df_mas_gam_lang_merg = pd.merge(df_mas_gam_lang, lang_code, left_on='key', right_on='639-1 code')
df_mas_gam_lang_merg = df_mas_gam_lang_merg.drop(['key','639-1 code'], axis=1)
#df_mas_gam_lang_merg = df_mas_gam_lang_merg[df_mas_gam_lang_merg['value']>100]
df_mas_gam_lang_merg_sort = df_mas_gam_lang_merg.sort_values(by='value', ascending=False)
top_values = df_mas_gam_lang_merg_sort.head(5)
other_sum = df_mas_gam_lang_merg_sort.iloc[5:]['value'].sum()
df_mas_gam_lang_merg_pie = pd.concat([top_values, pd.DataFrame({'ISO language name': ['Other'], 'value': [other_sum]})])

#ax = df_mas_gam_lang_merg.plot.bar(x='ISO language name', y='value', rot=0, figsize=(20, 5))
ax = df_mas_gam_lang_merg_pie.plot.pie(y='value', labels=df_mas_gam_lang_merg_pie['ISO language name'], autopct='%1.1f%%', pctdistance=0.8)
ax.set_xticklabels(ax.get_xticklabels(), rotation=45, ha='right')
plt.title('Ratio of toots related to gambiling in each language')
ax.get_legend().remove()
plt.ylabel('')
plt.xlabel('')
plt.savefig('./images/toot_lang.png')

au_po = pd.read_excel('../file/Australian Bureau of Statistics.xlsx')
au_po = au_po.drop([8,9,10,11,12])
au_po = au_po.rename(columns={'Unnamed: 0': 'state', au_po.columns[1]: 'count'})
au_po = au_po.sort_values(by='state')

ax = au_po.plot.pie(y='count', labels=au_po['state'], autopct='%1.1f%%', pctdistance=0.8)
#ax.set_xticklabels(ax.get_xticklabels(), rotation=45, ha='right')

plt.legend(au_po['state'], loc='center left', bbox_to_anchor=(1.2, 0.5))
ax.get_legend().remove()
plt.title('Ratio of population in each state')
plt.ylabel('')
plt.xlabel('')
plt.savefig('./images/state_popu.png')


# mas_count = pd.read_json('../file/mastodon_count.json').drop('comments', axis=1)
mas_count = pd.read_json(request("/mastodon_p/_design/no_cal/_view/count?reduce=true&group_level=2"))
if 'comments' in mas_count.columns:
    mas_count = mas_count.drop('comments', axis=1)
mas_count = pd.json_normalize(mas_count['rows'])
mas_count[['gambling', 'vulgarity']] = mas_count['key'].apply(pd.Series)
mas_count = mas_count.drop('key',axis=1)

mas_count_gam = mas_count.groupby('gambling').sum().reset_index()
mas_count_vul = mas_count.groupby('vulgarity').sum().reset_index()

ax = mas_count_gam.plot.pie(y='value', labels=['non-gambling', 'gambling'], autopct='%1.1f%%', pctdistance=0.8)
ax.get_legend().remove()
plt.title('Ratio of the gambling related toots')
plt.ylabel('')
plt.xlabel('')
plt.savefig('./images/toot_gambling.png')

ax = mas_count_vul.plot.pie(y='value', labels=['non-vulgarity', 'vulgarity'], autopct='%1.1f%%', pctdistance=0.8)
ax.get_legend().remove()
plt.title('Ratio of the vulgarity related toots')
plt.ylabel('')
plt.xlabel('')
plt.savefig('./images/toot_vulgarity.png')

# twt_count = pd.read_json('../file/count.json').drop('comments', axis=1)
twt_count = pd.read_json(request("/tweet/_design/no_cal/_view/count?reduce=true&group_level=2"))
if 'comments' in twt_count.columns:
    twt_count = twt_count.drop('comments', axis=1)
twt_count = pd.json_normalize(twt_count['rows'])
twt_count[['gambling', 'vulgarity']] = twt_count['key'].apply(pd.Series)
twt_count = twt_count.drop('key',axis=1)

twt_count_gam = twt_count.groupby('gambling').sum().reset_index()
twt_count_vul = twt_count.groupby('vulgarity').sum().reset_index()

ax = twt_count_gam.plot.pie(y='value', labels=['non-gambling', 'gambling'], autopct='%1.1f%%', pctdistance=0.8)
ax.get_legend().remove()
plt.title('Ratio of the gambling related tweets')
plt.ylabel('')
plt.xlabel('')
plt.savefig('./images/tweet_gambling.png')

ax = twt_count_vul.plot.pie(y='value', labels=['non-vulgarity', 'vulgarity'], autopct='%1.1f%%', pctdistance=0.8)
ax.get_legend().remove()
plt.title('Ratio of the vulgarity related tweets')
plt.ylabel('')
plt.xlabel('')
plt.savefig('./images/tweet_vulgarity.png')

# gcc_count = pd.read_json('../file/gcc_count.json')
gcc_count = pd.read_json(request("/tweet/_design/gcc/_view/gcc_count?reduce=true&group_level=2"))
gcc_count = pd.json_normalize(gcc_count['rows'])
gcc_count['key'] = gcc_count['key'].astype(float)
gcc_count = gcc_count.drop([3,4])
gcc_count = gcc_count.groupby('key').sum().reset_index()

ax = gcc_count.plot.pie(y='value', labels=['unknown place', 'not in GCC', 'in GCC'], autopct='%1.1f%%', pctdistance=0.8)
ax.get_legend().remove()
plt.title('Ratio of tweets location')
plt.ylabel('')
plt.xlabel('')
plt.savefig('./images/tweet_loc.png')

# vul_sen_area = pd.read_json('../file/vulgarity_sentiment_area.json')
vul_sen_area = pd.read_json(request("/tweet/_design/vulgar_sentiment/_view/vulgar_sentiment_area?reduce=true&group_level=2"))
vul_sen_area = pd.json_normalize(vul_sen_area['rows'])
vul_sen_area[['vul_cnt', 'sen', 'tot_cnt']] = vul_sen_area['value'].apply(pd.Series)
vul_sen_area = vul_sen_area.drop(['value'],axis=1)
vul_sen_area = vul_sen_area.drop(vul_sen_area[vul_sen_area['vul_cnt']==0].index)
vul_sen_area = vul_sen_area.rename(columns={'key': 'state'})
vul_sen_area = vul_sen_area.sort_values(by='state')

ax = vul_sen_area.plot.pie(y='vul_cnt', labels=vul_sen_area['state'], autopct='%1.1f%%', pctdistance=0.8)
ax.get_legend().remove()
plt.title('Ratio of vulgarity related tweets in each state')
plt.ylabel('')
plt.xlabel('')
plt.savefig('./images/tweet_state_vulgar_ratio.png')

