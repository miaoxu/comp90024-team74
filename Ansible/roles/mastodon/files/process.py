import json
import thulac
from thai_tokenizer import Tokenizer
import fugashi
from textblob import TextBlob
from gensim.utils import tokenize


lang_list = ['en', 'hu', 'mr', 'tl', 'vi', 'fr', 'da', 'lt', 'tr', 'my', 'ca', 'ru', 'ta', 'ro', 'sl', 'kn', 'bg', 'sd',
             'is', 'de', 'am', 'zh', 'iw', 'nl', 'th', 'es', 'lv', 'ko', 'te', 'hy', 'it', 'ne', 'et', 'cs', 'gu', 'ja',
             'cy', 'ml', 'pl', 'ar', 'pt', 'bn', 'ps', 'uk', 'el', 'ka', 'no', 'hi', 'fa', 'pa', 'fi', 'sr', 'eu', 'si',
             'sv', 'ht', 'ur']
# extra_attention=['zh', 'th','ja']


with open('config/gambling.json', encoding='utf-8') as f:
    gambling_vocab = json.load(f)
# gambling_vocab['en']

with open('config/vulgar.json', encoding='utf-8') as f:
    vulgar_vocab = json.load(f)
# vulgar_vocab['en']


def tweet(i):
    i['gambling'] = 0
    i['vulgar'] = 0
    i['token'] = []
    i['sentiment'] = -1  # unknown
    if i['language'] == 'zh':
        thu1 = thulac.thulac(seg_only=True)  # chinese
        i['token'] = [i[0] for i in thu1.cut(i['toot'])]
    elif i['language'] == 'th':
        tokenizer = Tokenizer()  # tai
        i['token'] = tokenizer.split(i['toot'])
    elif i['language'] == 'ja':
        tagger = fugashi.Tagger()  # japan
        i['token'] = [word.surface for word in tagger(i['toot'])]
    elif i['language'] in lang_list:
        i['token'] = list(set(list(tokenize(i['toot']))))

    ind = 0
    while (i['gambling'] == 0 or i['vulgar'] == 0) and ind < len(i['token']):
        word = i['token'][ind]
        if word in gambling_vocab[i['language']]:
            i['gambling'] = 1
        if word in vulgar_vocab[i['language']]:
            i['vulgar'] = 1
        ind += 1

    if i['language'] == 'en':
        blob = TextBlob(i['toot'])
        i['sentiment'] = (blob.sentiment.polarity + 1) / 2

    return i


if __name__ == '__main__':
    p = dict()
    p['toot'] = 'today is a nice day'
    p['language'] = 'en'
    print(tweet(p))

    p = dict()
    p['toot'] = '他赌博输了很多钱'
    p['language'] = 'zh'
    print(tweet(p))
