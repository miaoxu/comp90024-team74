# COMP90024 Team 74
# Di Liu(1399095) - Melbourne, AU - leo.liu5@student.unimelb.edu.au
# Miao Xu(662824) - Wuxi, CHN - miaox1@student.unimelb.edu.au
# Yinshao Chen(1273974) - Melbourne, AU - yinshao.chen@student.unimelb.edu.au
# Wei Zhang(1426188) - Melbourne, AU - wei.zhang12@student.unimelb.edu.au
# Wanying Ding(1050097) - Melbourne, AU - wddin@student.unimelb.edu.au

from mastodon import Mastodon, StreamListener
from bs4 import BeautifulSoup
from auth import auth_dict, db_url
import sys
import couchdb
from process import tweet

# get the target server and api info
target_server = sys.argv[1]
db_auth = 'http://admin:admin@{}:5984'.format(db_url)

# initial a mastodon api connection
m = Mastodon(
    api_base_url=auth_dict[target_server]['api_base_url'],
    access_token=auth_dict[target_server]['access_token']
)


# stream mastodon statuses, process and save to couchdb
class Listener(StreamListener):
    def __init__(self, **parameters):
        self.db_target = None
        self.load_param(**parameters)

    def load_param(self, **parameters):
        self.db_target = parameters['db_target']

    def on_update(self, status):
        language = status['language']
        usr_id = status['account']['id']
        create_time = str(status['created_at'])
        tags = ','.join([k['name'] for k in status['tags']])
        toot = status['content']
        soup = BeautifulSoup(toot, "lxml")
        content = ' '.join([k.text for k in soup.find_all('p')])
        data_obj = {'usr_id': usr_id, 'create_time': create_time, 'toot': content, 'tag': tags, 'language': language,
                    'source': target_server}
        processed_obj = tweet(data_obj)
        try:
            self.db_target.save(processed_obj)
        except couchdb.http.ResourceConflict as e:
            print(e)
            pass


# connect to the CouchDB
dbserver = couchdb.Server(db_auth)
if 'mastodon_p' in dbserver:
    db_mastodon = dbserver['mastodon_p']
else:
    db_mastodon = dbserver.create('mastodon_p')

parameter = {
    'db_target': db_mastodon
}

# start the harvester
harvester = Listener(**parameter)
m.stream_public(harvester)


